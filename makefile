SRC=src/astbuilder.cpp src/atom.cpp src/function.cpp src/list.cpp src/main.cpp src/number.cpp \
	src/runcontext.cpp src/bool.cpp src/arithmetic.cpp src/comparison.cpp src/control.cpp
OBJ=$(SRC:.cpp=.o)
CXX=g++
CXXFLAGS=-Wall -Wextra -pedantic -std=c++11

all:compile doc

compile:sanchgui

sanchgui:$(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS)

doc:
	doxygen Doxyfile

clean:
	rm src/*.o
	rm sanchgui
	rm -rf doc

run:sanchgui
	./sanchgui
