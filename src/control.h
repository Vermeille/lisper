#pragma once

#include <iostream>
#include <stdexcept>

#include "number.h"
#include "astbuilder.h"
#include "list.h"
#include "atom.h"
#include "bool.h"
#include "runcontext.h"
#include "function.h"

#include "control.h"

/**
 * @brief if construction (if cond exp1 exp2) evaluates and returns exp1 only
 * if cond, exp2 otherwise
 *
 * @param l cond, exp1 and exp2
 * @param ctx context
 *
 * @return exp1 or exp2 evaluated
 */
std::shared_ptr<Value> If(std::shared_ptr<List> l, RunContext& ctx);

/**
 * @brief let construction (let ((name1 val1) (name2 val2) ...) code)
 * binds names to values, executes code then unbind (local variables)
 *
 * @param l a list of couples, and code
 * @param ctx context
 *
 * @return code evaluated
 */
std::shared_ptr<Value> Let(std::shared_ptr<List> l, RunContext& ctx);

/**
 * @brief defines a function (defun fun (arg1 arg2 ...) code)
 *
 * @param l fun, list of arguments and code
 * @param ctx context
 *
 * @return returns nothing
 */
std::shared_ptr<Value> Defun(std::shared_ptr<List> l, RunContext& ctx);

/**
 * @brief sequence of instructions and returns the last
 * (do instr1 instr2 instr3 ...). Executes sequentielly instr1/2/3 and returns
 * the result of the last
 *
 * @param l instructions
 * @param ctx context
 *
 * @return last instruction evaluated
 */
std::shared_ptr<Value> Do(std::shared_ptr<List> l, RunContext& ctx);
