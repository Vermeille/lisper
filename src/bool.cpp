#include "bool.h"
#include "value.h"

    Bool::Bool(bool b)
: _val(b)
{
}

bool Bool::val() const
{
    return _val;
}

void Bool::Print() const
{
    std::cout << ((_val) ? "T " : "Nil ");
}

std::shared_ptr<Value> Bool::Eval(std::shared_ptr<List>, RunContext&) const
{
    return std::make_shared<Bool>(_val);
}

