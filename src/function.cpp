#include <memory>

#include "list.h"
#include "value.h"
#include "runcontext.h"
#include "function.h"

    HardFun::HardFun(F f)
: _f(f)
{
}

    std::shared_ptr<Value>
HardFun::Eval(std::shared_ptr<List> params, RunContext& ctx) const
{
    List* i = params.get();
    auto new_list = std::make_shared<List>();

    while (i)
    {
        new_list->Append(i->val()->Eval(nullptr, ctx));
        i = i->next();
    }

    return _f(new_list, ctx);
}

    SpecialFun::SpecialFun(F f)
: _f(f)
{
}

    std::shared_ptr<Value>
SpecialFun::Eval(std::shared_ptr<List> params, RunContext& ctx) const
{
    return _f(params, ctx);
}


    UserFun::UserFun(std::vector<std::string> args, std::shared_ptr<Value> code)
: _args(args), _code(code)
{
}
    std::shared_ptr<Value>
UserFun::Eval(std::shared_ptr<List> params, RunContext& ctx) const
{
    RunContext::Context nctx;

    List* i = params.get();
    for (auto& arg : _args)
    {
        nctx[arg] = i->val()->Eval(nullptr, ctx);
        i = i->next();
    }

    ctx.PushScope(std::move(nctx));
    auto ret =_code->Eval(nullptr, ctx);
    ctx.PopScope();
    return ret;
}
