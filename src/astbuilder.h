#pragma once

#include <iostream>
#include <string>

class Value;

/**
 * @brief Builds the AST
 */
class ASTBuilder
{
    public:
        /**
         * @brief constructor : take the stream and eats the first '('
         *
         * @param in
         */
        ASTBuilder(std::istream& in);

        /**
         * @brief Builds the AST
         *
         * @return the AST
         */
        Value* Build();

    private:
        /**
         * @brief Get next token from stream
         *
         * @return the token
         */
        std::string GetToken();
        /**
         * @brief The stream
         */
        std::istream& _strm;
};

