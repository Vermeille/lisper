#pragma once

#include <map>
#include <deque>
#include <string>
#include <memory>

#include "list.h"
#include "value.h"

/**
 * @brief A stack of context (ie, std::map<Identifier, Value*>)
 */
class RunContext
{
    public:
        typedef std::map<std::string, std::shared_ptr<Value>> Context;
        typedef std::deque<Context> Contexts;

        /**
         * @brief Creates the stack and pushes the global context
         */
        RunContext();

        /**
         * @brief Sets an identifier if found
         *
         * @param id the name of the identifier
         * @param val the new value
         */
        void SetIdentifier(std::string id, Value* val);

        /**
         * @brief Same function overloaded.
         *
         * @param id
         * @param val
         */
        void SetIdentifier(std::string id, std::shared_ptr<Value> val);
        /**
         * @brief Looks in the Context and returns the value associated with
         * the identifier
         *
         * @param id the identifier
         *
         * @return context[id]
         */
        std::shared_ptr<Value> GetIdentifier(std::string id) const;

        /**
         * @brief Adds a context on the top of the Contexts stack
         *
         * @param ctx the new context
         */
        void PushScope(Context ctx = Context());
        /**
         * @brief Pops the context on the top of the stack
         */
        void PopScope();

    private:
        Contexts _envs;
};
