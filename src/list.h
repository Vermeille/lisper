#pragma once

#include <memory>

#include "value.h"

class RunContext;

/**
 * @brief A Lisp list
 */
class List : public Value
{
    public:
        /**
         * @brief a useless destructor
         */
        ~List();

        /**
         * @brief Constructor (useless too)
         */
        List();
        /**
         * @brief Prints the list
         */
        virtual void Print() const override;

        /**
         * @brief Evaluates the list
         *
         * @param unused (give it nullptr)
         * @param ctx context
         *
         * @return result
         */
        virtual std::shared_ptr<Value>
            Eval(std::shared_ptr<List> l, RunContext& ctx) const override;

        /**
         * @brief Appends a value to the list
         *
         * @param val
         */
        void Append(Value* val);

        /**
         * @brief Appends a value to the list
         *
         * @param val
         */
        void Append(std::shared_ptr<Value> val);
        /**
         * @brief Next element of the list
         *
         * @return pointer to the next
         */
        List* next() const;
        /**
         * @brief Next element of the list
         *
         * @return pointer to the next
         */
        std::shared_ptr<List> pnext() const;
        /**
         * @brief value holded by the head is returned
         *
         * @return the value holded by the head of the list
         */
        std::shared_ptr<Value> val() const;

    private:
        std::shared_ptr<List>  _next;
        std::shared_ptr<Value> _val;
};
