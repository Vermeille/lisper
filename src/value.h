#pragma once

#include <memory>

#include "list.h"

class RunContext;
class List;

/**
 * @brief Base class for all runtime values
 */
class Value
{
    public:
        /**
         * @brief virtual destructor
         */
        virtual ~Value() {}

        /**
         * @brief Abstract print
         */
        virtual void Print() const = 0;
        /**
         * @brief Abstract eval
         *
         * @param params parameters for evaluations (needed for functions)
         * @param ctx context (needed for translating atoms)
         *
         * @return the value evaluated
         */
        virtual std::shared_ptr<Value> Eval(std::shared_ptr<List> params,
                RunContext& ctx) const = 0;
};
