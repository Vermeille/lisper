#pragma once

#include <memory>
#include <vector>
#include <string>

#include "list.h"
#include "value.h"
#include "runcontext.h"

/**
 * @brief Abstract function class
 */
class Function : public Value
{
    public:
        virtual void Print() const override {}
};

/**
 * @brief Hardcoded function
 */
class HardFun : public Function
{
    public:
        typedef std::shared_ptr<Value> (*F)(const std::shared_ptr<List>, const RunContext&);

        /**
         * @brief Constructor
         *
         * @param f pointer to function
         */
        HardFun(F f);

        /**
         * @brief evaluates the function
         *
         * @param params parameters
         * @param ctx context
         *
         * @return function evaluated
         */
        virtual std::shared_ptr<Value>
            Eval(std::shared_ptr<List> params, RunContext& ctx) const override;

    private:
        F _f;
};

/**
 * @brief Function wich does not follow the regular evaluation rule
 */
class SpecialFun : public Function
{
    public:
        typedef std::shared_ptr<Value> (*F)(std::shared_ptr<List>, RunContext&);

        /**
         * @brief constructor
         *
         * @param f pointer to function
         */
        SpecialFun(F f);
        /**
         * @brief Evaluates the function
         *
         * @param params parameters (can be evaluated by the function or not)
         * @param ctx context
         *
         * @return result
         */
        virtual std::shared_ptr<Value> Eval(std::shared_ptr<List> params, RunContext& ctx) const override;

    private:
        F _f;
};

/**
 * @brief User written function
 */
class UserFun : public Function
{
    public:
        /**
         * @brief Constructor
         *
         * @param args arguments names
         * @param code the code
         */
        UserFun(std::vector<std::string> args, std::shared_ptr<Value> code);

        /**
         * @brief Evaluates the function
         *
         * @param params parameters (can be evaluated by the function or not)
         * @param ctx context
         *
         * @return result
         */
        virtual std::shared_ptr<Value>
            Eval(std::shared_ptr<List> params, RunContext& ctx) const override;

    private:
        std::vector<std::string> _args;
        std::shared_ptr<Value> _code;
};
