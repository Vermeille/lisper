/*
** control.cpp for lisper
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  lun. 21 mai 2012 19:43:15 CEST Guillaume "Vermeille" Sanchez
** Last update mer. 23 mai 2012 09:58:49 CEST Guillaume "Vermeille" Sanchez
*/

#include <iostream>
#include <stdexcept>

#include "number.h"
#include "astbuilder.h"
#include "list.h"
#include "atom.h"
#include "bool.h"
#include "runcontext.h"
#include "function.h"

#include "control.h"

std::shared_ptr<Value> If(std::shared_ptr<List> l, RunContext& ctx)
{
    auto cond = l->val()->Eval(nullptr, ctx);

    if (Bool* b = dynamic_cast<Bool*>(cond.get()))
    {
        if (b->val())
            return l->pnext()->val()->Eval(nullptr, ctx);
        else
            return l->pnext()->pnext()->val()->Eval(nullptr, ctx);
    }
    else
        throw std::runtime_error("First argument of if should be boolean value");
}

std::shared_ptr<Value> Let(std::shared_ptr<List> l, RunContext& ctx)
{
    RunContext::Context nctx;

    if (List* vars = dynamic_cast<List*>(l->val().get()))
    {
        while (vars)
        {
            List* couple = dynamic_cast<List*>(vars->val().get());

            Atom* id = dynamic_cast<Atom*>(couple->val().get());
            std::shared_ptr<Value> val = couple->next()->val()->Eval(nullptr, ctx);

            nctx[id->str()] = val;
            val->Print();

            vars = vars->next();
        }
        ctx.PushScope(std::move(nctx));
        auto ret = l->next()->val()->Eval(nullptr, ctx);
        ctx.PopScope();
        return ret;
    }
    else
        throw std::runtime_error("Let's first argument should be a list");
}

std::shared_ptr<Value> Defun(std::shared_ptr<List> l, RunContext& ctx)
{
    std::string fun_name = dynamic_cast<Atom*>(l->val().get())->str();
    std::vector<std::string> args;

    List* p = dynamic_cast<List*>(l->next()->val().get());

    while (p)
    {
        args.push_back(dynamic_cast<Atom*>(p->val().get())->str());
        p = p->next();
    }

    auto fun = std::make_shared<UserFun>(args, l->next()->pnext()->val());

    ctx.SetIdentifier(fun_name, fun);

    return nullptr;
}

std::shared_ptr<Value> Do(std::shared_ptr<List> l, RunContext& ctx)
{
    List* p = l.get();

    std::shared_ptr<Value> res;

    while (p)
    {
        res = p->val()->Eval(nullptr, ctx);
        p = p->next();
    }

    return res;
}
