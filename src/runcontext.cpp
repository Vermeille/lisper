#include <stdexcept>
#include <iostream>

#include "runcontext.h"

RunContext::RunContext()
{
    PushScope();
}

void RunContext::SetIdentifier(std::string id, Value* val)
{
    for (auto& env : _envs)
    {
        auto it = env.find(id);
        if (it != std::end(env))
        {
            (it)->second = std::shared_ptr<Value>(val);
        }
    }

    _envs[0][id] = std::shared_ptr<Value>(val);
}

void RunContext::SetIdentifier(std::string id, std::shared_ptr<Value> val)
{
    for (auto& env : _envs)
    {
        auto it = env.find(id);
        if (it != std::end(env))
        {
            (it)->second = val;
        }
    }

    _envs[0][id] = val;
}

std::shared_ptr<Value> RunContext::GetIdentifier(std::string id) const
{
    for (auto& env : _envs)
    {
        Context::const_iterator it = env.find(id);
        if (it != std::end(env))
            return it->second;
    }
    throw std::runtime_error(id + " does not exist");
}

void RunContext::PushScope(Context ctx)
{
    _envs.push_front(ctx);
}

void RunContext::PopScope()
{
    _envs.pop_front();
}
