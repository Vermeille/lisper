#pragma once

#include <memory>

#include "number.h"
#include "astbuilder.h"
#include "list.h"
#include "atom.h"
#include "bool.h"
#include "runcontext.h"
#include "function.h"

/**
 * @brief Equality test (= a b) => a == b
 *
 * @param l values
 * @param ctx context
 *
 * @return boolean value
 */
std::shared_ptr<Value> Eq(const std::shared_ptr<List> l, const RunContext& ctx);

/**
 * @brief Less test (less a b) => a < b
 *
 * @param l a and b
 * @param ctx context
 *
 * @return boolean value
 */
std::shared_ptr<Value> Less(const std::shared_ptr<List> l, const RunContext& ctx);

/**
 * @brief Lessor equality (leq a b) =>  a <= b
 *
 * @param l a and b
 * @param ctx context
 *
 * @return boolean value
 */
std::shared_ptr<Value> Leq(const std::shared_ptr<List> l, const RunContext& ctx);

/**
 * @brief greater test (greater a b) => a > b
 *
 * @param l and b
 * @param ctx context
 *
 * @return boolean value
 */
std::shared_ptr<Value> Greater(const std::shared_ptr<List> l, const RunContext& ctx);

/**
 * @brief greater or equal test (geq a b) => a >= b
 *
 * @param l a and b
 * @param ctx context
 *
 * @return boolean value
 */
std::shared_ptr<Value> Geq(const std::shared_ptr<List> l, const RunContext& ctx);

