#pragma once

#include <memory>

#include "number.h"
#include "astbuilder.h"
#include "list.h"
#include "atom.h"
#include "bool.h"
#include "runcontext.h"
#include "function.h"

/**
 * @brief Substraction (min a b c) => a - b - c
 *
 * @param l list of arguments
 * @param ctx context and environment
 *
 * @return result
 */
std::shared_ptr<Value> Min(const std::shared_ptr<List> l, const RunContext& ctx);

/**
 * @brief Multiplication (mul a b c) => a * b * c
 *
 * @param l list of arguments
 * @param ctx context and environment
 *
 * @return  result
 */
std::shared_ptr<Value> Mul(const std::shared_ptr<List> l, const RunContext& ctx);

/**
 * @brief (plus a b c) => a + b + c
 *
 * @param l list of arguments
 * @param ctx context
 *
 * @return result
 */
std::shared_ptr<Value> Add(const std::shared_ptr<List> l, const RunContext& ctx);
