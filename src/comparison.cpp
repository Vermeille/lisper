/*
** comparison.cpp for lisper
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  lun. 21 mai 2012 18:12:10 CEST Guillaume "Vermeille" Sanchez
** Last update mar. 22 mai 2012 02:50:13 CEST Guillaume "Vermeille" Sanchez
*/

#include <iostream>
#include <stdexcept>

#include "number.h"
#include "astbuilder.h"
#include "list.h"
#include "atom.h"
#include "bool.h"
#include "runcontext.h"
#include "function.h"

#include "comparison.h"

std::shared_ptr<Value> Eq(const std::shared_ptr<List> l, const RunContext&)
{
    int v = dynamic_cast<Number*>(l->val().get())->val();
    List * p = l->next();

    while (p)
    {
        if (v != (dynamic_cast<Number*>(p->val().get())->val()))
            return std::make_shared<Bool>(false);

        p = p->next();
    }

    return std::make_shared<Bool>(true);
}

std::shared_ptr<Value> Less(const std::shared_ptr<List> l, const RunContext&)
{
    int v = dynamic_cast<Number*>(l->val().get())->val();
    return std::make_shared<Bool>(v < dynamic_cast<Number*>(l->pnext()->val().get())
            ->val());
}

std::shared_ptr<Value> Leq(const std::shared_ptr<List> l, const RunContext&)
{
    int v = dynamic_cast<Number*>(l->val().get())->val();
    return std::make_shared<Bool>(v <= dynamic_cast<Number*>(l->pnext()->val().get())
            ->val());
}

std::shared_ptr<Value> Greater(const std::shared_ptr<List> l, const RunContext&)
{
    int v = dynamic_cast<Number*>(l->val().get())->val();
    return std::make_shared<Bool>(v > dynamic_cast<Number*>(l->pnext()->val().get())
            ->val());
}

std::shared_ptr<Value> Geq(const std::shared_ptr<List> l, const RunContext&)
{
    int v = dynamic_cast<Number*>(l->val().get())->val();
    return std::make_shared<Bool>(v >= dynamic_cast<Number*>(l->pnext()->val().get())
            ->val());
}

