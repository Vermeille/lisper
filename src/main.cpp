/*
** main.cpp for lisper
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  sam. 19 mai 2012 19:37:20 CEST Guillaume "Vermeille" Sanchez
** Last update mer. 23 mai 2012 09:52:38 CEST Guillaume "Vermeille" Sanchez
*/

#include <iostream>
#include <stdexcept>

#include "number.h"
#include "astbuilder.h"
#include "list.h"
#include "atom.h"
#include "bool.h"
#include "runcontext.h"
#include "function.h"
#include "arithmetic.h"
#include "comparison.h"
#include "control.h"

std::shared_ptr<Value> Car(const std::shared_ptr<List> l, const RunContext&)
{
    if (List* p = dynamic_cast<List*>(l->val().get()))
    {
        return p->val();
    }
    else
        throw std::runtime_error("car can only be applied on lists");
}

std::shared_ptr<Value> Cdr(const std::shared_ptr<List> l, const RunContext&)
{
    if (List* p = dynamic_cast<List*>(l->val().get()))
    {
        return p->pnext();
    }
    else
        throw std::runtime_error("cdr can only be applied on lists");
}

std::shared_ptr<Value> Print(std::shared_ptr<List> l, RunContext&)
{
    List* p = l.get();

    while (p)
    {
        auto v = p->val();
        v->Print();
        p = p->next();
    }
    return nullptr;
}

std::shared_ptr<Value> Quote(std::shared_ptr<List> l, RunContext&)
{
    return l->val();
}


int main()
{
    RunContext ctx;
    ctx.SetIdentifier("plus", new HardFun(&Add));
    ctx.SetIdentifier("mul", new HardFun(&Mul));
    ctx.SetIdentifier("min", new HardFun(&Min));
    ctx.SetIdentifier("car", new HardFun(&Car));
    ctx.SetIdentifier("cdr", new HardFun(&Cdr));
    ctx.SetIdentifier("print", new SpecialFun(&Print));
    ctx.SetIdentifier("quote", new SpecialFun(&Quote));
    ctx.SetIdentifier("let", new SpecialFun(&Let));
    ctx.SetIdentifier("if", new SpecialFun(&If));
    ctx.SetIdentifier("defun", new SpecialFun(&Defun));
    ctx.SetIdentifier("do", new SpecialFun(&Do));
    ctx.SetIdentifier("t", new Bool(true));
    ctx.SetIdentifier("nil", new Bool(false));
    ctx.SetIdentifier("eq", new HardFun(&Eq));
    ctx.SetIdentifier("leq", new HardFun(&Leq));
    ctx.SetIdentifier("less", new HardFun(&Less));
    ctx.SetIdentifier("greater", new HardFun(&Greater));
    ctx.SetIdentifier("geq", new HardFun(&Geq));
    ASTBuilder ast(std::cin);
    Value* v = ast.Build();
    v->Print();
    std::cout << "\n====== EVALUATION TIME ======\n";
    std::shared_ptr<Value> r = v->Eval(nullptr, ctx);
    r->Print();
    return 0;
}

