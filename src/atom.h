#pragma once

#include <memory>
#include <string>

#include "list.h"
#include "value.h"

class RunContext;

/**
 * @brief An atom, an identifier
 */
class Atom : public Value
{
    public:
        /**
         * @brief Constructor
         *
         * @param val the name of the Atom
         */
        Atom(std::string val);

        /**
         * @brief Print the atom
         */
        virtual void Print() const override;

        /**
         * @brief Evaluates the atom (ie, look in the context and return the
         * value)
         *
         * @param l unused
         * @param ctx unused
         *
         * @return the value associated to tjis atom
         */
        virtual std::shared_ptr<Value>
            Eval(std::shared_ptr<List>, RunContext& ctx) const override;

        /**
         * @brief returns the string represeting the atom
         *
         * @return the name of the atom
         */
        std::string str() const;

    private:
        std::string _val;
};
