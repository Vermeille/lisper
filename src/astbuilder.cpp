#include <ctype.h>

#include "astbuilder.h"
#include "list.h"
#include "number.h"
#include "atom.h"

    ASTBuilder::ASTBuilder(std::istream& in)
: _strm(in)
{
    GetToken();
}

Value* ASTBuilder::Build()
{
    std::string tok;
    List* l = new List;

    while (tok != ")")
    {
        if (tok == "(")
            l->Append(Build());
        else if (isdigit(tok[0]))
            l->Append(new Number(tok));
        else if (isalpha(tok[0]))
            l->Append(new Atom(tok));

        tok = GetToken();
    }

    return l;
}

std::string ASTBuilder::GetToken()
{
    char c = _strm.get();

    while (isspace(c))
        c = _strm.get();

    std::string token;

    token += c;

    if (c == ')' || c == '(')
    {
        return token;
    }

    while (isalnum(_strm.peek()))
        token += _strm.get();

    return token;
}

