#include <iostream>
#include <sstream>

#include "number.h"

Number::Number(std::string repr)
{
    std::istringstream iss(repr);
    iss >> _val;
}

Number::Number(int val)
    : _val(val)
{
}

void Number::Print() const
{
    std::cout << "Nbr:" << _val << " ";
}

std::shared_ptr<Value> Number::Eval(std::shared_ptr<List>, RunContext&) const
{
    return std::shared_ptr<Value>(new Number(_val));
}

int Number::val() const
{
    return _val;
}

