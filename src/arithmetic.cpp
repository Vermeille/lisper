/*
** main.cpp for lisper
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  sam. 19 mai 2012 19:37:20 CEST Guillaume "Vermeille" Sanchez
** Last update mer. 23 mai 2012 09:48:20 CEST Guillaume "Vermeille" Sanchez
*/

#include <iostream>
#include <stdexcept>

#include "number.h"
#include "astbuilder.h"
#include "list.h"
#include "atom.h"
#include "bool.h"
#include "runcontext.h"
#include "function.h"

#include "arithmetic.h"

std::shared_ptr<Value> Add(const std::shared_ptr<List> l, const RunContext&)
{
    int res = 0;

    List* lp = l.get();

    while (lp != nullptr)
    {
        std::shared_ptr<Value> v = lp->val();

        if (Number* n = dynamic_cast<Number*>(v.get()))
        {
            res += n->val();
        }
        else
            throw std::runtime_error("Not a number argument for +");

        lp = lp->next();
    }
    return std::shared_ptr<Value>(new Number(res));
}

std::shared_ptr<Value> Mul(const std::shared_ptr<List> l, const RunContext&)
{
    int res = 1;

    List* lp = l.get();

    while (lp != nullptr)
    {
        std::shared_ptr<Value> v = lp->val();

        if (Number* n = dynamic_cast<Number*>(v.get()))
        {
            res *= n->val();
        }
        else
            throw std::runtime_error("Not a number argument for *");

        lp = lp->next();
    }
    return std::shared_ptr<Value>(new Number(res));
}

std::shared_ptr<Value> Min(const std::shared_ptr<List> l, const RunContext&)
{
    int res = dynamic_cast<Number*>(l->val().get())->val();

    List* lp = l->next();

    while (lp != nullptr)
    {
        std::shared_ptr<Value> v = lp->val();

        if (Number* n = dynamic_cast<Number*>(v.get()))
        {
            res -= n->val();
        }
        else
            throw std::runtime_error("Not a number argument for *");

        lp = lp->next();
    }
    return std::shared_ptr<Value>(new Number(res));
}

