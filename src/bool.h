#pragma once

#include <iostream>
#include <memory>

#include "list.h"
#include "value.h"

/**
 * @brief A Boolean runtime value
 */
class Bool : public Value
{
    public:
        /**
         * @brief Constructor
         *
         * @param b initial value
         */
        Bool(bool b);

        /**
         * @brief the boolean value wrapped
         *
         * @return the value
         */
        bool val() const;

        /**
         * @brief Prints "T" or "Nil" (respectively true false)
         */
        virtual void Print() const override;
        /**
         * @brief returns the value itself
         *
         * @param unused
         * @param unused
         *
         * @return the value
         */
        std::shared_ptr<Value> Eval(std::shared_ptr<List>, RunContext&) const override;

    private:
        bool _val;
};
