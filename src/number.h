#pragma once

#include <memory>

#include "list.h"

/**
 * @brief A Lisp runtime Number
 */
class Number : public Value
{
    public:
        /**
         * @brief "123123" constructor. Takes a string
         *
         * @param repr string representing a number
         */
        Number(std::string repr);

        /**
         * @brief Constructs the Number around val
         *
         * @param val the wrapper int
         */
        Number(int val);

        /**
         * @brief Prints the number
         */
        virtual void Print() const override;

        /**
         * @brief Returns a copy of the number
         *
         * @param l unused
         * @param ctx unused
         *
         * @return copy of this instance
         */
        virtual std::shared_ptr<Value> Eval(std::shared_ptr<List> l,
                RunContext& ctx) const override;
        /**
         * @brief returns the underlying int
         *
         * @return the int
         */
        int val() const;

    private:
        int _val;
};
