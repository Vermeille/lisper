#include <iostream>
#include <stdexcept>

#include "list.h"
#include "atom.h"
#include "function.h"
#include "runcontext.h"

void List::Print() const
{
    if (!_val)
    {
        std::cout << " () ";
        return;
    }

    const List* l = this;
    std::cout << " (";

    while (l)
    {
        l->_val->Print();
        l = l->_next.get();
    }
    std::cout << ") ";
}

    std::shared_ptr<Value>
List::Eval(std::shared_ptr<List>, RunContext& ctx) const
{
    Atom* atom = dynamic_cast<Atom*>(_val.get());
    auto val = ctx.GetIdentifier(atom->str());

    if (Function* f = dynamic_cast<Function*>(val.get()))
    {
        return f->Eval(_next, ctx);
    }
    else
        throw std::runtime_error(atom->str() + " is not a function");
}

void List::Append(Value* val)
{
    if (!_val)
    {
        _val = std::shared_ptr<Value>(val);
        return;
    }

    List* l = this;

    while (l->_next)
        l = l->_next.get();

    l->_next = std::make_shared<List>();
    l->_next->_val = std::shared_ptr<Value>(val);
}

void List::Append(std::shared_ptr<Value> val)
{
    if (!_val)
    {
        _val = val;
        return;
    }

    List* l = this;

    while (l->_next)
        l = l->_next.get();

    l->_next = std::make_shared<List>();
    l->_next->_val = val;
}
List* List::next() const
{
    return _next.get();
}

std::shared_ptr<List> List::pnext() const
{
    return _next;
}

std::shared_ptr<Value> List::val() const
{
    return _val;
}

List::~List()
{
}

List::List()
    : _next(nullptr), _val(nullptr)
{
}
