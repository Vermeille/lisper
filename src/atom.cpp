#include <iostream>

#include "atom.h"
#include "runcontext.h"

    Atom::Atom(std::string val)
: _val(val)
{
}

void Atom::Print() const
{
    std::cout << "Atom:" << _val << " ";
}

std::shared_ptr<Value> Atom::Eval(std::shared_ptr<List>, RunContext& ctx) const
{
    return ctx.GetIdentifier(_val);
}

std::string Atom::str() const
{
    return _val;
}

